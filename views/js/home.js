"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//enum
const gLOGIN_URL = "login.html";
const gBASE_URL = "/api/devcamp-lucky-dice/";
const gCONTENT_TYPE = { "Content-Type": "application/json;charset=UTF-8" };
const gSUCCESS_TOKEN = { x_success_token: "" };
const gSUCCESS_TOKEN_EXPIRED = "Access Token has expired";
const gREFRESH_TOKEN_EXPIRED = "Refresh token has expired";

// Biến để theo dõi số lần thắng liên tiếp
//khai báo ở ngoài để không bị ảnh hưởng mỗi lần tung xúc xắc
var gWinCout = 0;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();

  $("#btn").click(onBtnRollClick);

  $("#log-link").click(onLogLinkClick);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//  Hàm tải trang
async function onPageLoading() {
  try {
    //lấy accesstoken trong session
    let tokenObj = JSON.parse(Cookies.get("token"));
    if (tokenObj) {
      ////nếu có thì gán cho biến toàn cục để đưa vào api
      gSUCCESS_TOKEN.x_success_token = tokenObj.accessToken;
      const response = await callAPIGetInfoUser();
      const data = await response.json();
      if (response.ok) {
        handleLoggedIn(data);
      } else {
        if (
          response.status === 401 &&
          data.message === gSUCCESS_TOKEN_EXPIRED
        ) {
          //accesstoken hết hạn
          await checkExpiredToken();
          return;
        }
        throw new Error(data.message); //401, 404 ko báo lỗi
      }
    } else {
      //nếu ko có
      window.location.href = gLOGIN_URL; //quay về trang đăng nhập
    }
  } catch (error) {
    console.error(error);
    alert(error.message);
    setTimeout(() => {
      window.location.href = gLOGIN_URL; //quay về trang đăng nhập
    }, 3000);
  }
}

//Khai báo hàm xử lý sự kiện bấm nút ném
async function onBtnRollClick() {
  try {
    const user = {
      username: null,
      firstname: null,
      lastname: null,
    };
    //B1: Thu thập
    collectUserInfo(user);
    //B2: Validate
    let vCheck = validateUserInfo(user);
    if (vCheck) {
      //B3: Xử lý
      const response = await callAPIGetNewDice(user);
      const data = await response.json();
      handleGetNewDiceSuccess(data);
    }
  } catch (error) {
    console.log(error);
    alert(error.message);
  }
}

//Khai báo hàm xử lý sự kiện bấm log
function onLogLinkClick() {
  //Kiểm tra trạng thái đăng nhập
  if (gSUCCESS_TOKEN.x_success_token) {
    handleLoggedOut(); //nếu đã đăng nhập thì xử lý, ko thì thôi
  } else {
    window.location.href = gLOGIN_URL;
  }
}

//Hàm check thời hạn token
async function checkExpiredToken() {
  try {
    //lấy refreshToken
    let tokenObj = JSON.parse(Cookies.get("token"));
    let refreshToken = { refreshToken: tokenObj.refreshToken };
    //callAPI refreshToken
    const response = await callAPIRefreshToken(refreshToken);
    const data = await response.json();
    if (response.ok) {
      handleRefreshToken(data);
    } else {
      if (
        response.status === 401 &&
        data.message === gREFRESH_TOKEN_EXPIRED
      ) {
        //Refresh token hết hạn
        alert("Đã hết thời hạn đăng nhập");
        setTimeout(() => {
          window.location.href = gLOGIN_URL; //quay về trang đăng nhập
        }, 3000);
        return;
      }
      throw new Error(data.message); //401, 404 ko báo lỗi
    }
  } catch (error) {
    console.error(error);
    alert(error.message);
  }
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//API
async function callAPIGetInfoUser() {
  let response = await fetch(gBASE_URL + "user-info", {
    method: "get",
    headers: { ...gCONTENT_TYPE, ...gSUCCESS_TOKEN },
  });
  return response;
}

async function callAPIGetNewDice(paramUser) {
  const response = await fetch(gBASE_URL + "dice", {
    method: "post",
    headers: { ...gCONTENT_TYPE, ...gSUCCESS_TOKEN },
    body: JSON.stringify(paramUser),
  });
  return response;
}

async function callAPIRefreshToken(paramRefreshToken) {
  const vREFRESH_URL = "/api/auth/refresh-token";
  let response = await fetch(vREFRESH_URL, {
    method: "post",
    headers: gCONTENT_TYPE,
    body: JSON.stringify(paramRefreshToken),
  });
  return response;
}

//hàm xử lý đăng nhập
function handleLoggedIn(paramData) {
  $("#input-username").val(paramData.username);
  $("#input-firstname").val(paramData.firstname);
  $("#input-lastname").val(paramData.lastname);
  $("#log-link").html("Đăng xuất");
}

// Hàm xử lý đăng xuất
function handleLoggedOut() {
  // Xóa thông tin đăng nhập từ localStorage
  Cookies.remove("token");
  Cookies.remove("username");
  Cookies.remove("firstname");
  Cookies.remove("lastname");

  // Đặt giá trị của các trường input về rỗng
  $("#input-username").val("");
  $("#input-firstname").val("");
  $("#input-lastname").val("");

  // Cập nhật nội dung của nút "Đăng nhập"
  $("#log-link").html("Đăng nhập");
}

//Hàm xử lý get new dice success
function handleGetNewDiceSuccess(paramResult) {
  console.log(paramResult);
  //gọi 2 hàm thay đổi ảnh và lời nhắn
  changeImage(paramResult.dice);
  changeMessage(paramResult);

  //gọi hàm check điều kiện thắng
  let vCheck = winCheck(paramResult.dice);
  //gọi hàm reset khi thỏa mãn điều kiện thắng
  resetGame(vCheck);
}

//hanle refresh
function handleRefreshToken(paramData) {
  Cookies.set("token", JSON.stringify(paramData));
  onPageLoading();
}

//Hàm thu thập dữ liệu user
function collectUserInfo(paramUser) {
  paramUser.username = $.trim($("#input-username").val());
  paramUser.firstname = $.trim($("#input-firstname").val());
  paramUser.lastname = $.trim($("#input-lastname").val());
}

//Hàm kiểm tra dữ liệu user
function validateUserInfo(paramUser) {
  if (!paramUser.username && !paramUser.firstname && !paramUser.lastname) {
    alert("Bạn phải đăng nhập để chơi");
    return false;
  }
  if (!paramUser.username) {
    alert("username không hợp lệ");
    return false;
  }
  if (!paramUser.firstname) {
    alert("firstname không hợp lệ");
    return false;
  }
  if (!paramUser.lastname) {
    alert("lastname không hợp lệ");
    return false;
  }
  return true;
}

//hàm kiểm tra điều kiện thắng(6 lần liên tiếp được 6)
function winCheck(paramDice) {
  //vòng lặp so sánh kết quả xúc xắc với 6
  if (paramDice === 6) {
    gWinCout++;
    console.log("Số lần thắng: " + gWinCout);
    //nếu số lần xúc xắc ra 6 = 6 thì thắng, reset số lần đếm và thông báo ng chơi
    //đặt if trong if để đảm bảo điều kiện xúc xắc = 6 được thỏa mãn rồi mới tính đến số lần
    if (gWinCout === 6) {
      gWinCout = 0;
      alert("Tuyệt vời! Bạn đã thắng! Chúng tôi đã phá sản @_@");
      return true;
    }
  }
  //còn nếu ko bằng 6 thì cứ reset số lần đếm để đảm bảo phải 3 lần liên tục mới thắng
  else {
    gWinCout = 0;
  }
  return false;
}

//tạo hàm thay đổi ảnh xúc xắc
function changeImage(paramDice) {
  $("#dice").attr("src", `images/${paramDice}.png`);
}
//tạo hàm thay đổi thông điệp
function changeMessage(paramResult) {
  "use strict";
  $("#message").show();

  if (paramResult.dice >= 1 && paramResult.dice <= 3) {
    $("#message").html("Cảm ơn bạn. Hãy cố gắng lần sau !");
  } else {
    $("#message").html("Chúc mừng bạn. Hãy chơi ván nữa !");
  }

  if (paramResult.voucher !== null) {
    $("#message").append(
      `<br>Xin chúc mừng, bạn đã nhận được 1 voucher giảm giá ${paramResult.voucher.discount}% cho lần chơi tiếp theo`
    );
  }

  if (paramResult.prize !== null) {
    $("#message").append(
      `<br>Xin chúc mừng, bạn đã nhận được 1 phần thưởng là ${paramResult.prize.name}`
    );
  }
}

//tạo hàm reset khi thắng
function resetGame(paramBoolean) {
  if (paramBoolean == true) {
    $("#dice").attr("src", "images/dice.png");
    $("#message").html("").hide();
  }
}
