const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Voucher = new Schema(
  {
    code: {
      type: String,
      unique: true,
      required: true,
    },
    discount: {
      type: Number,
      required: true,
    },
    note: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const VoucherModel = mongoose.model("Voucher", Voucher);

module.exports = VoucherModel;
