const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Prize = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true,
    },
    description: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const PrizeModel = mongoose.model("Prize", Prize);

module.exports = PrizeModel;
