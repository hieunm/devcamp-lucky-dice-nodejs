const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const DiceHistory = new Schema(
  {
    user: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true,
    },
    dice: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const DiceHistoryModel = mongoose.model("DiceHistory", DiceHistory);

module.exports = DiceHistoryModel;
