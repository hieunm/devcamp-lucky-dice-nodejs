const express = require('express');
const randomNumber = require('../controllers/random-number.controller');

const RandomNumberRouter = express.Router();

RandomNumberRouter.get("/", randomNumber);

module.exports = RandomNumberRouter;