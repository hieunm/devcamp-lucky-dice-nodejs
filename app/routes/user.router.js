const express = require('express');

const {
  verifyToken,
  checkRoleUser,
} = require("../middlewares/author.middleware");

const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
} = require('../controllers/user.controller');

const UserRouter = express.Router();

UserRouter.post('/', [verifyToken, checkRoleUser], createUser);

UserRouter.get('/', [verifyToken, checkRoleUser], getAllUsers);

UserRouter.get('/:userId', [verifyToken, checkRoleUser], getUserById);

UserRouter.put('/:userId', [verifyToken, checkRoleUser], updateUserById);

UserRouter.delete('/:userId', [verifyToken, checkRoleUser], deleteUserById);

module.exports = UserRouter;