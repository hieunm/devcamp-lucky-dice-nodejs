const express = require("express");

const db = require("../models");
const {
  checkDuplicateSignup,
  checkExistedLogin,
} = require("../middlewares/authen.middleware");
const { signup, login, refreshToken } = require("../controllers/auth.controller");

const AuthRouter = express.Router();

AuthRouter.post("/signup", checkDuplicateSignup, signup);

AuthRouter.post("/login", checkExistedLogin, login);

AuthRouter.post("/refresh-token", refreshToken);

module.exports = AuthRouter;