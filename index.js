//thư viện
const express = require("express");
const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");
require("dotenv").config();

//data
const initializeData = require("./app/data");

//import router
//for user
const AuthRouter = require("./app/routes/auth.router");
const DiceRouter = require("./app/routes/devcamp-lucky-dice.router");
//for admin
const RandomNumberRouter = require("./app/routes/random-number.router");
const UserRouter = require("./app/routes/user.router");
const DiceHistoryRouter = require("./app/routes/dice-history.router");
const PrizeRouter = require("./app/routes/prize.router");
const PrizeHistoryRouter = require("./app/routes/prize-history.router");
const VoucherRouter = require("./app/routes/voucher.router");
const VoucherHistoryRouter = require("./app/routes/voucher-history.router");

//import middlewares dùng chung
const logTimeAPIRequest = require("./app/middlewares/time.controllers");
const logMethodAPIRequest = require("./app/middlewares/method.controller");

//biến
const app = express();
const port = process.env.SERVER_PORT || 8000;

//áp dụng middleware
app.use(cors());
app.use(express.static("views"));
app.use(express.json());
app.use(logTimeAPIRequest, logMethodAPIRequest);

//áp dụng router api
//for user
app.use("/api/auth", AuthRouter);
app.use("/api/devcamp-lucky-dice", DiceRouter);
//for admin
app.use("/api/random-number", RandomNumberRouter);
app.use("/api/users", UserRouter);
app.use("/api/dice-histories", DiceHistoryRouter);
app.use("/api/prizes", PrizeRouter);
app.use("/api/prize-histories", PrizeHistoryRouter);
app.use("/api/vouchers", VoucherRouter);
app.use("/api/voucher-histories", VoucherHistoryRouter);

//định nghĩa router cho trang home UI
app.get("/", (req, res) => {
  const indexPath = path.join(__dirname, "./views/home.html");
  res.sendFile(indexPath);
});

//kết nối mongoose
mongoose
  .connect(process.env.DB_DEVELOPMENT)
  .then(() => {
    initializeData(); //khởi tạo data
    console.log("Successfully connected to MongoDB");
  })
  .catch((error) => console.log(error.message));

//Khởi tạo cổng lắng nghe của server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

module.exports = app;
