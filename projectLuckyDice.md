# Project Lucky Dice

## 🌟 Code Explanation

> ### 1. Front-End

- **_Trang chủ(Home Page)_**
- Khi tải trang, sẽ lấy dữ liệu biến isLoggedIn trên localStorage để kiểm tra trạng thái đăng nhập. Nếu đã đăng nhập thì lấy thông tin user trên localStorage rồi xử lý hiển thị.
- Khi click nút ném, sẽ thực hiện 3 bước xử lý sự kiện(thu thập, kiểm tra và callAPI). Nếu chưa đăng nhập thì sẽ yêu cầu đăng nhập.
- Sau khi callAPI lấy được thông tin xúc xắc sẽ xử lý hiển thị tương ứng với kết quả tung xúc xắc.
- Ngay sau đó sẽ kiểm tra điều kiện thắng dựa theo biến gWinCout. Nếu tung được 6 điểm thì sẽ cộng thêm 1 vào biến gWinCout, còn ko phải 6 thì sẽ reset gWinCout = 0. gWinCout = 6 thì sẽ thắng và reset lại kết quả chơi.
- Khi bấm vào nút đăng nhập/đăng xuất, kiểm tra isLoggedIn = true thì sẽ xử lý đăng xuất, còn isLoggedIn = false thì sẽ chuyển hướng tới trang đăng nhập.
- Khi bấm vào nút lịch sử thì sẽ chuyển hướng tới trang kiểm tra lịch xử tung xúc xắc.
- **_Trang đăng nhập(Login Page)_**
- Khi bấm vào nút đăng nhập thì sẽ thực hiện 3 bước xử lý sự kiện(thu thập, kiểm tra và callAPI). Call API kiểm tra thông tin đăng nhập rồi tiến hành xử lý hiển thị. Nếu đúng thì sẽ chuyển hướng tới trang chủ.
- Khi người dùng chưa có account thì sẽ bấm nút sign up, chuyển hướng tới trang đăng ký.
- **_Trang đăng ký(Sign Up Page)_**
- Khi bấm vào nút đăng xuất thì sẽ thực hiện 3 bước xử lý sự kiện(thu thập, kiểm tra và callAPI). Call API tạo mới tài khoản rồi tiến hành xử lý hiển thị. Nếu tạo thành công thì sẽ chuyển hướng tới trang đăng nhập.
- Khi người dùng đã có account thì sẽ bấm nút login, chuyển hướng tới trang đăng nhập.
- **_Trang lịch sử(Histories Page)_**
- Có 3 trang kiểm tra lịch sử: Lịch sử tung xúc xắc, Lịch sử voucher, Lịch sử phần thưởng
- Khi bấm nút show histories thì sẽ thực hiện 3 bước xử lý sự kiện(thu thập, kiểm tra và callAPI). Call API kiểm tra thông tin username rồi tiến hành xử lý hiển thị.
- Nếu tìm thấy username sẽ tiến hành đổ dữ liệu lịch sử vào bảng.
- Khi bấm vào biểu tượng Lucky Dice sẽ chuyển hướng tới trang chủ.
> ### 2. Back-End
- **_Models_**
- Import thư viện Mongoose
- Khai báo đối tượng Schema
- Khởi tạo một Schema trong Mongoose với các thuộc tính cần thiết.
- Tạo 1 Model đối với Schema vừa khởi tạo và exports.
> ### 2. Controllers
- Import thư viện Mongoose và các Model cần thiết.
- Định nghĩa các hàm bất đồng bộ để xử lý các yêu cầu call API.
- Các queries trong Mongoose là bất đồng bộ nên sẽ cần sử dụng await trước mỗi truy vấn query.
- Đặt toàn bộ hàm trong khối try-catch để bắt lỗi hiệu quả tránh treo server khi xảy ra lỗi bất ngờ.
- Nếu đối tượng có các trường tham chiếu đến Model khác, sử dụng populate() để lấy thông tin đầy đủ(có thể populate() lồng nhau hoặc nhiều Model 1 lúc).
- Mỗi khi có lỗi xảy ra thì sẽ xử lý phản hồi tới client và code sẽ ngừng ngay lập tức.
- Một số status chính dùng để phản hồi tới client là:
  > - 200: Khi response trả về thành công
  > - 201: (Chỉ dành cho POST) Khi tạo dữ liệu thành công
  > - 204: (Chỉ dành cho DELETE) Khi xóa thành công. Ta cũng có thể sử dụng status 200 cho DELETE nếu như muốn phản hồi thông điệp về cho client
  > - 400: Khi input truyền vào từ request (query, params hoặc body JSON) không hợp lệ
  > - 404: Khi không tìm thấy bản ghi trên CSDL
  > - 500: Khi code bị lỗi
- Trong mỗi hàm sẽ có thứ tự xử lý như sau:
  > - _Bước 1: Thu thập dữ liệu_
  > - _Bước 2: Kiểm tra dữ liệu_
  > - _Bước 3: Xử lý dữ liệu_
- Sẽ có các controller cơ bản như sau:
  > - _Get Alls_
  > - _Get By Username, By Id_
  > - _Create_
  > - _Update_
  > - _Delete_
  >
> ### 3. Middlewares
- Middleware in ra method của request.
- Middleware in ra thời gian của request.
> ### 4. Routes
- Import thư viện express
- Import controllers vừa tạo sử dụng phép gán phá hủy cấu trúc đối tượng
- Khởi tạo 1 router bằng express
- Dùng phương thức router.<method>() (trong đó <method> là phương thức HTTP như post, get, put, delete) để định nghĩa các endpoint API và xác định các handlers(controllers) cho chúng
- Gán các hàm xử lý (handlers) tương ứng cho mỗi endpoint API
- Định nghĩa đường dẫn cho mỗi endpoint sử dụng format Restful API
> ### 5. App
- File khởi tạo chính của dự án
- Import các thư viện express, mongoose, path, cors
- Import các router
- Định nghĩa các biến app và port
- Thêm middleware express.json(), express.static(), cors() và 2 custom middleware vừa định nghĩa.
- Trỏ đường dẫn tới tới file home page
- Định nghĩa các router API
- Kết nối với MongoDB
- Khởi tạo dự án chạy trong môi trường NodeJs, dữ liệu server sẽ được lắng nghe tại cổng 8000(port) để tránh nhầm lẫn với các cổng hệ thống.

## 📄 Description

> ### 1. Tổng quan

- Trang bắt đầu của dự án là trang Home Page
- Người dùng sẽ cần tạo account và đăng nhập để có thể chơi
- Ngoài ra có thể dùng username để tìm kiếm lịch sử chơi
> ### 2. Cách tổ chức thư mục dự án
- Sử dụng mô hình MVC (Model-View-Controller) trong NodeJs để tổ chức thư mục dự án.
- File khởi tạo chính của dự án là _index.js_.
- Thư mục app chứa 4 tổ chức file chính đó là _routes_, _middleware_, _controllers_, _models_.
- Các file trong dự án liên kết với nhau theo chuẩn CommonJS.
> ### 3. Luồng thực thi của dự án
- Dự án sẽ được bắt đầu từ file chính index.js
- Client sẽ sử dụng API để tương tác với server
- API sau khi được gọi sẽ lần lượt chạy qua routes -> middlewares -> controllers -> CSDL.

## 🧱 Technology

> ### Front-end:
> 1. [Bootstrap 4](https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css)
> 2. [Jquery 3](https://code.jquery.com/jquery-3.7.1.js)
> 3. [Font Awesome 5](https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css)
> 4. JSON
> 5. Javascript
> 5. Local Storage

> ### Back-end:
> 1. [Node.js](https://nodejs.org/en)
> 2. [Express.js(4.18.3)](https://expressjs.com/)
> 3. [Mongoose.js(8.2.1)](https://mongoosejs.com/)
> 4. [Cors(2.8.5)](https://expressjs.com/en/resources/middleware/cors.html)
> 5. [UUID(9.0.1)](https://www.npmjs.com/package/uuid)
> 6. [Nodemon(3.1.0)](https://www.npmjs.com/package/nodemon)

> ### Database:
> 1. [NoSQL - MongoDB](https://www.mongodb.com/)
